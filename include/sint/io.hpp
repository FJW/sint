
#ifndef SINT_IO_HPP
#define SINT_IO_HPP

#include <istream>
#include <ostream>

#include "core.hpp"

namespace sint {

template<typename Char, typename Traits, std::size_t B, sign S>
std::basic_istream<Char, Traits>& operator>>(std::basic_istream<Char, Traits>& s, integer<B, S>& i) {
	typename integer<B, S>::base_type tmp = {};
	if (s >> tmp) {
		i.set_value(tmp);
	}
	return s;
}

template<typename Char, typename Traits, std::size_t B, sign S>
std::basic_ostream<Char, Traits>& operator<<(std::basic_ostream<Char, Traits>& s, integer<B, S> i) {
	// Add plus, so that i8 and u8 will be printed as numbers:
	return s << +(i.get_value());
}


} // namespace sint

#endif
