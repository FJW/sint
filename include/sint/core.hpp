
#ifndef SINT_CORE_HPP
#define SINT_CORE_HPP

#include <cstdint>
#include <stdexcept>
#include <limits>

namespace sint {

enum class sign {
	has_sign,
	no_sign
};

class arithmetic_error: public std::runtime_error {
	using std::runtime_error::runtime_error;
};

namespace impl {

template<std::size_t Bits, sign Sign>
struct base_integer;

template<> struct base_integer<8,  sign::has_sign>{ using type = std::int8_t; };
template<> struct base_integer<16, sign::has_sign>{ using type = std::int16_t; };
template<> struct base_integer<32, sign::has_sign>{ using type = std::int32_t; };
template<> struct base_integer<64, sign::has_sign>{ using type = std::int64_t; };
template<> struct base_integer<8,  sign::no_sign>{ using type = std::uint8_t; };
template<> struct base_integer<16, sign::no_sign>{ using type = std::uint16_t; };
template<> struct base_integer<32, sign::no_sign>{ using type = std::uint32_t; };
template<> struct base_integer<64, sign::no_sign>{ using type = std::uint64_t; };


template<std::size_t Bits, sign Sign>
using base_integer_t = typename base_integer<Bits,Sign>::type;

} // namespace impl


template<std::size_t Bits, sign Sign>
class integer {
public:
	using base_type = impl::base_integer_t<Bits, Sign>;

	constexpr integer(): m_value{0} {}
	constexpr explicit integer(base_type value): m_value{value} {}

	constexpr base_type get_value() const { return m_value; }
	constexpr void set_value(base_type value) { m_value = value; }

	constexpr const static base_type min = std::numeric_limits<base_type>::min();
	constexpr const static base_type max = std::numeric_limits<base_type>::max();
	constexpr const static bool is_signed = (Sign == sign::has_sign);
private:
	base_type m_value;
};


template<typename Ret, std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
Ret add(integer<Bl, Sl> l, integer<Br, Sr> r) {
	typename Ret::base_type tmp;
	if(__builtin_add_overflow(l.get_value(), r.get_value(), &tmp)) {
		throw arithmetic_error{"integer-overflow in add-operation"};
	}
	return Ret{tmp};
}


template<typename Ret, std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
Ret sub(integer<Bl, Sl> l, integer<Br, Sr> r) {
	typename Ret::base_type tmp;
	if(__builtin_sub_overflow(l.get_value(), r.get_value(), &tmp)) {
		throw arithmetic_error{"integer-overflow in sub-operation"};
	}
	return Ret{tmp};
}

template<typename Ret, std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
Ret mul(integer<Bl, Sl> l, integer<Br, Sr> r) {
	typename Ret::base_type tmp;
	if(__builtin_mul_overflow(l.get_value(), r.get_value(), &tmp)) {
		throw arithmetic_error{"integer-overflow in mul-operation"};
	}
	return Ret{tmp};
}

// Since picking a sane common-type is rather hard, we will limit use of operators
// to lhs and rhs having the same type and the assignment-version:

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator+(integer<Bits, Sign> l, integer<Bits, Sign> r) {
	return add<integer<Bits, Sign>>(l, r);
}

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator-(integer<Bits, Sign> l, integer<Bits, Sign> r) {
	return sub<integer<Bits, Sign>>(l, r);
}

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator*(integer<Bits, Sign> l, integer<Bits, Sign> r) {
	return mul<integer<Bits, Sign>>(l, r);
}


template<std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
integer<Bl, Sl>& operator+=(integer<Bl, Sl>& l, integer<Br, Sr> r) {
	return l = l+r;
}

template<std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
integer<Bl, Sl>& operator-=(integer<Bl, Sl>& l, integer<Br, Sr> r) {
	return l = l-r;
}

template<std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
integer<Bl, Sl>& operator*=(integer<Bl, Sl>& l, integer<Br, Sr> r) {
	return l = l*r;
}

// For completeness sake:
template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator+(integer<Bits,Sign> arg) {
	return arg;
}

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator-(integer<Bits,Sign> arg) {
	// This catches INTMIN for signed values:
	return integer<Bits, Sign>{0} - arg;
}

// And now for the problem-children: division and modulo:
// TODO: catch other cases than rhs being 0:

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator/(integer<Bits, Sign> l, integer<Bits, Sign> r) {
	using integer = integer<Bits, Sign>;
	// we don't have comparisson yet, so let's fall back to the
	// base-types for this check:
	if (!r.get_value() or (r.is_signed and r.get_value() == -1 and l.get_value() == l.min)) {
		throw arithmetic_error{"Division by zero"};
	}
	// This cast will always work except for the case of INT_MIN / -1 which
	// is already caught above:
	return integer{static_cast<typename integer::base_type>(l.get_value() / r.get_value())};
}

template<std::size_t Bits, sign Sign>
integer<Bits, Sign> operator%(integer<Bits, Sign> l, integer<Bits, Sign> r) {
	using integer = integer<Bits, Sign>;
	if (!r.get_value()) {
		throw arithmetic_error{"Modulo of zero"};
	}
	// TODO: is this correct?
	return integer{static_cast<typename integer::base_type>(l.get_value() % r.get_value())};
}

template<std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
integer<Bl, Sl>& operator/=(integer<Bl, Sl>& l, integer<Br, Sr> r) {
	return l = l/r;
}

template<std::size_t Bl, sign Sl, std::size_t Br, sign Sr>
integer<Bl, Sl>& operator%=(integer<Bl, Sl>& l, integer<Br, Sr> r) {
	return l = l%r;
}

// Finally, comparisson and equality; We only allow the comparission
// for equal types and between types and it's bases:

// First: both sides are safe integers:

template<std::size_t B, sign S>
bool operator==(integer<B, S> l, integer<B, S> r) {
	return l.get_value() == r.get_value();
}

template<std::size_t B, sign S>
bool operator!=(integer<B, S> l, integer<B, S> r) {
	return !(l == r);
}

template<std::size_t B, sign S>
bool operator<(integer<B, S> l, integer<B, S> r) {
	return l.get_value() < r.get_value();
}

template<std::size_t B, sign S>
bool operator<=(integer<B, S> l, integer<B, S> r) {
	return !(r < l);
}

template<std::size_t B, sign S>
bool operator>(integer<B, S> l, integer<B, S> r) {
	return r < l;
}

template<std::size_t B, sign S>
bool operator>=(integer<B, S> l, integer<B, S> r) {
	return !(l < r);
}

// second only the left side is a safe integer:
// In order to reduce confusion, we won't forward everything
// to == and < this time:
template<std::size_t B, sign S>
bool operator==(integer<B, S> l, typename integer<B, S>::base_type r) {
	return l.get_value() == r;
}

template<std::size_t B, sign S>
bool operator!=(integer<B, S> l, typename integer<B, S>::base_type r) {
	return !(l == r);
}

template<std::size_t B, sign S>
bool operator<(integer<B, S> l, typename integer<B, S>::base_type r) {
	return l.get_value() < r;
}

template<std::size_t B, sign S>
bool operator<=(integer<B, S> l, typename integer<B, S>::base_type r) {
	return l.get_value() <= r;
}

template<std::size_t B, sign S>
bool operator>(integer<B, S> l, typename integer<B, S>::base_type r) {
	return l.get_value() > r;
}

template<std::size_t B, sign S>
bool operator>=(integer<B, S> l, typename integer<B, S>::base_type r) {
	return l.get_value() >= r;
}

// Finally with the left side as basic value:

template<std::size_t B, sign S>
bool operator==(typename integer<B, S>::base_type l, integer<B, S> r) {
	return l == r.get_value();
;
}

template<std::size_t B, sign S>
bool operator!=(typename integer<B, S>::base_type l, integer<B, S> r) {
	return !(l == r.get_value());
}

template<std::size_t B, sign S>
bool operator<(typename integer<B, S>::base_type l, integer<B, S> r) {
	return l < r.get_value();
}

template<std::size_t B, sign S>
bool operator<=(typename integer<B, S>::base_type l, integer<B, S> r) {
	return l <= r.get_value();
}

template<std::size_t B, sign S>
bool operator>(typename integer<B, S>::base_type l, integer<B, S> r) {
	return l > r.get_value();
}

template<std::size_t B, sign S>
bool operator>=(typename integer<B, S>::base_type l, integer<B, S> r) {
	return l >= r.get_value();
}

// We want aliases:

using i8  = integer<8,  sign::has_sign>;
using i16 = integer<16, sign::has_sign>;
using i32 = integer<32, sign::has_sign>;
using i64 = integer<64, sign::has_sign>;

using u8  = integer<8,  sign::no_sign>;
using u16 = integer<16, sign::no_sign>;
using u32 = integer<32, sign::no_sign>;
using u64 = integer<64, sign::no_sign>;

using pd = integer<sizeof(std::size_t) * 8, sign::has_sign>;
using sz = integer<sizeof(std::size_t) * 8, sign::no_sign>;



} // namspace sint


#endif
