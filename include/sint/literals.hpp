#ifndef SINT_LITERALS_HPP
#define SINT_LITERALS_HPP

#include <cstdint>

#include "core.hpp"

namespace sint {

namespace impl {
// Whoever decided that the only way to get the
// argument as a constexpr was to do this: You did bad
// and you should feel bad!!

template<std::uintmax_t X, std::uintmax_t Y, std::uintmax_t Z>
struct mul_add {
	// TODO: static asserts:
	enum: std::uintmax_t {value = X*Y + Z};
};

template<char C, unsigned Base>
struct to_i {
	enum : unsigned {
		value = ('0' <= C and C <= '9') ? C - '0':
			('a' <= C and C <= 'z') ? C - 'a' + 10:
			('A' <= C and C <= 'Z') ? C - 'a' + 10:
			Base
	};
	static_assert(value < Base, "");
};

template<unsigned Base, char H, char...T>
struct parse_as_base{
	enum: std::uintmax_t {
		factor = parse_as_base<Base, T...>::factor * Base,
		value = mul_add<to_i<H, Base>::value, factor, parse_as_base<Base, T...>::value>::value
	};
};

template<unsigned Base, char C>
struct parse_as_base<Base, C>{
	enum: std::uintmax_t {
		factor = 1,
		value = to_i<C, Base>::value
	};
};

template<char...C>
struct to_integer {
	enum: std::uintmax_t {value = parse_as_base<10, C...>::value};
};

template<char...C>
struct to_integer<'0', C...> {
	enum: std::uintmax_t {value = parse_as_base<8, C...>::value};
};

template<char...C>
struct to_integer<'0', 'x', C...> {
	enum: std::uintmax_t {value = parse_as_base<16, C...>::value};
};

template<char...C>
struct to_integer<'0', 'b', C...> {
	enum: std::uintmax_t {value = parse_as_base<2, C...>::value};
};

template<>
struct to_integer<'0'> {
	enum: std::uintmax_t {value = 0};
};

} // namespace impl

inline namespace literals {

template <char...C>
i8 operator""_i8() {
	return i8{impl::to_integer<C...>::value};
}

template <char...C>
i16 operator""_i16() {
	return i16{impl::to_integer<C...>::value};
}

template <char...C>
i32 operator""_i32() {
	return i32{impl::to_integer<C...>::value};
}

template <char...C>
i64 operator""_i64() {
	return i64{impl::to_integer<C...>::value};
}

template <char...C>
u8 operator""_u8() {
	return u8{impl::to_integer<C...>::value};
}

template <char...C>
u16 operator""_u16() {
	return u16{impl::to_integer<C...>::value};
}

template <char...C>
u32 operator""_u32() {
	return u32{impl::to_integer<C...>::value};
}

template <char...C>
u64 operator""_u64() {
	return u64{impl::to_integer<C...>::value};
}

template <char...C>
pd operator""_pd() {
	return pd{impl::to_integer<C...>::value};
}

template <char...C>
sz operator""_sz() {
	return sz{impl::to_integer<C...>::value};
}


} // namespace literals


} // namespace sint


#endif
