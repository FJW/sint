
#include <cstdint>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <sint/sint.hpp>

BOOST_AUTO_TEST_SUITE( sint_literals )

BOOST_AUTO_TEST_CASE(decimal) {
	using namespace sint;
	BOOST_CHECK_EQUAL(123_u8, u8{123});
	BOOST_CHECK(124_u8 != u8{123});
	BOOST_CHECK_EQUAL(-123_i8, i8{-123});
	BOOST_CHECK_EQUAL(-127_i8, i8{-127});
}

BOOST_AUTO_TEST_CASE(octal) {
	using namespace sint;
	BOOST_CHECK_EQUAL(0123_u8, u8{0123});
	BOOST_CHECK(0124_u8 != u8{0123});
	BOOST_CHECK_EQUAL(-0123_i8, i8{-0123});
	BOOST_CHECK_EQUAL(-0127_i8, i8{-0127});
}

BOOST_AUTO_TEST_CASE(hex) {
	using namespace sint;
	BOOST_CHECK_EQUAL(0x1f_u8, u8{0x1f});
	BOOST_CHECK(0xff_u8 != u8{0xfe});
	BOOST_CHECK_EQUAL(-0x7f_i8, i8{-0x7f});
}

BOOST_AUTO_TEST_CASE(binary) {
	using namespace sint;
	BOOST_CHECK_EQUAL(0b10_u8, u8{0b10});
	BOOST_CHECK_EQUAL(0b1001_i8, i8{0b1001});
	BOOST_CHECK_EQUAL(-0b1001_i8, i8{-0b1001});
}



BOOST_AUTO_TEST_SUITE_END()
