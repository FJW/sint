
#include <cstdint>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <sint/sint.hpp>

BOOST_AUTO_TEST_SUITE( sint_core )

BOOST_AUTO_TEST_CASE( creation) {
	BOOST_CHECK_NO_THROW( sint::i8{} );
	BOOST_CHECK_NO_THROW( sint::i8{23} );
	sint::i8 mvar{42};
	const sint::i8 cvar{42};
	BOOST_CHECK_NO_THROW( sint::i8{mvar} );
	BOOST_CHECK_NO_THROW( sint::i8{cvar} );
	BOOST_CHECK_NO_THROW( mvar = sint::i8{47} );
	BOOST_CHECK_NO_THROW( mvar = cvar );
}

BOOST_AUTO_TEST_CASE( add ) {
	BOOST_CHECK_EQUAL(23, sint::i16{12} + sint::i16{11});
	BOOST_CHECK_THROW(sint::i8{100} + sint::i8{28}, sint::arithmetic_error);
}

BOOST_AUTO_TEST_CASE( sub ) {
	BOOST_CHECK_EQUAL(23, sint::i16{42} - sint::i16{19});
	BOOST_CHECK_THROW(sint::i8{-10} - sint::i8{120}, sint::arithmetic_error);
}

BOOST_AUTO_TEST_CASE( mul ) {
	BOOST_CHECK_EQUAL(42, sint::i16{6} * sint::i16{7});
	BOOST_CHECK_THROW(sint::i8{-10} * sint::i8{120}, sint::arithmetic_error);
}

BOOST_AUTO_TEST_CASE( div ) {
	BOOST_CHECK_EQUAL(6, sint::i16{42} / sint::i16{7});
	BOOST_CHECK_EQUAL(2, sint::i16{23} / sint::i16{10});
	BOOST_CHECK_THROW(sint::i8{-10} / sint::i8{0}, sint::arithmetic_error);
}

BOOST_AUTO_TEST_CASE( mod ) {
	BOOST_CHECK_EQUAL(0, sint::i16{42} % sint::i16{7});
	BOOST_CHECK_EQUAL(3, sint::i16{23} % sint::i16{20});
	BOOST_CHECK_THROW(sint::i8{-10} % sint::i8{0}, sint::arithmetic_error);
}

BOOST_AUTO_TEST_SUITE_END()


