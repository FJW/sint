
#include <iostream>

#include <sint/sint.hpp>

int main() {
	sint::i8 var1{23};
	sint::i32 var2{16};
	auto var3 = var2;
	std::cout << var2 + var3 << '\n';

	using namespace sint::literals;
	const auto var4 = 127_i8;
	std::cout << var4 << '\n';
	// Error as it should be:
	//const auto var5 = 128_i8;
}
